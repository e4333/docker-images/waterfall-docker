# This file is part of BungeeCord-Docker.
#
# BungeeCord-Docker is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or any later version.
#
# BungeeCord-Docker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BungeeCord-Docker. If not, see <https://www.gnu.org/licenses/>.

FROM openjdk:17-alpine
LABEL org.opencontainers.image.authors="ultraxime@yahoo.fr"

RUN apk update && apk add tmux rsync su-exec xterm bash curl

ENV BUNGEE_HOME="/opt/bungee"
ENV BUNGEE_SRC="/usr/src/bungee"
ENV PLUGINS_DIR=BUNGEE_HOME"/plugins"

RUN addgroup -S bungee && \
    adduser -G bungee -S bungee -H && \
    touch /run/first_time && \
    mkdir -p $BUNGEE_HOME $BUNGEE_SRC $PLUGINS_DIR && \
    echo "set -g status off" > /root/.tmux.conf

ARG BUNGEE_URL="https://ci.md-5.net/job/BungeeCord/lastSuccessfulBuild/artifact/bootstrap/target/BungeeCord.jar"
ENV BUNGEE_JAR="BungeeCord.jar"

SHELL [ "/bin/bash", "-c" ]

RUN echo "Downloading Bungee" \
    && curl -S $BUNGEE_URL -o "$BUNGEE_SRC/$BUNGEE_JAR"

ARG SCRIPT="bungee"

COPY $SCRIPT /usr/local/bin/
COPY . $BUNGEE_SRC

EXPOSE 25577


VOLUME [ $BUNGEE_HOME, $PLUGINS_DIR ]

ENTRYPOINT ["/usr/local/bin/bungee"]
CMD ["run"]

HEALTHCHECK --interval=20s --timeout=2s --start-period=20s --retries=3 \
    CMD bungee healthcheck
